from reinfection_model import ReinfectionModel
from transmission_model import TransmissionModel


def diagnosed_infected(model):
    """Computes for each t the number of diagnosed infected."""
    return model["IQ"] + model["IQa"]


def diagnosed_infected_per_1000(model):
    """Computes for each t the number of diagnosed infected per 1000 people."""
    peak = diagnosed_infected(model)
    return (peak / model["NN"]) * 1000.0


def peak_infected_per_1000(model):
    infected = diagnosed_infected_per_1000(model)
    return infected.max()


def peak_deaths(model):
    """Peak deaths."""
    return model["DD"].max()


def peak_attack_rate(model):
    return model["DD"].max()


def t_to_peak(model):
    """Computes days to the peak of diagnosed infected."""
    peak = diagnosed_infected(model)
    peak_max_idx = peak.argmax()
    t = model.iloc[peak_max_idx]["t"]
    return t * 365.0


def attack_rate(model):
    n_tot = model["S"].iloc[0] + model["IS"].iloc[0]  # total population
    return (model["RQ"] + model["DD"]) / n_tot


def peak_attack_rate(model):
    """
    Computes peak attack rate in percent.
    Attack rate is the proportion of the population who died or recovered after an severe infection.
    """
    return attack_rate(model).max() * 100.0


if __name__ == "__main__":
    measures = ["Baseline", "Mask", "Hand", "Individual", "Gov"]

    for measure in measures:
        print(f"Comparison of measure '{measure}'")
        trans = TransmissionModel(intervention=measure)
        trans.set_baseline()
        trans = trans.train()
        
        reinf = ReinfectionModel(0.0, 0.0, intervention=measure)
        reinf.set_baseline()
        reinf = reinf.train()
        
        print(
            "Peak infection:",
            peak_infected_per_1000(trans),
            peak_infected_per_1000(reinf),
        )

        print("Peak attack rate:", peak_attack_rate(trans), peak_attack_rate(reinf))
        print("Peak deaths:", peak_deaths(trans), peak_deaths(reinf))
        print("Days until peak infection:", t_to_peak(trans), t_to_peak(reinf))
        print()
