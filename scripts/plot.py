import multiprocessing

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
from evaluation import *
from palettable.colorbrewer.qualitative import Set2_7
from scipy.signal import find_peaks

colors = Set2_7.mpl_colors
params = {
    "axes.labelsize": 14,
    "font.size": 14,
    "legend.fontsize": 10,
    "xtick.labelsize": 10,
    "ytick.labelsize": 10,
    "text.usetex": False,
    "figure.figsize": [8, 4],
}
plt.rcParams.update(params)


def pretty_plots(ax=None, figsize=[8, 4]):
    if ax is None:
        plt.grid(color="0.9", linestyle="-", linewidth=1)
        legend = plt.legend()
    else:
        ax.grid(color="0.9", linestyle="-", linewidth=1)
        legend = ax.legend()

    frame = legend.get_frame()
    frame.set_facecolor("1.0")
    frame.set_edgecolor("1.0")


def plot_infected(rw, sw, label="", linestyle="-", color=colors[0]):
    reinfection_model = ReinfectionModel(sw=sw, rw=rw)
    model = reinfection_model.train()

    infected = diagnosed_infected_per_1000(model)
    if sw != 0:
        peaks = get_peaks(infected)
        print("Peaks t (months)")
        print(model.t[peaks] * 12)
        mean_peaks = np.mean(np.diff(reinfection_model.t[peaks] * 12))
        print(f"Mean time between peaks: {round(mean_peaks, 2)} months")
        # plt.plot(reinfection_model.t[peaks] * 12, infected[peaks], "x")
    print()

    plt.plot(
        reinfection_model.t * 12,
        infected,
        color=color,
        linewidth=2,
        linestyle=linestyle,
        label=label,
    )


def get_peaks(infected):
    low_prom, _ = find_peaks(infected, prominence=(None, 0.5))
    all_peaks, _ = find_peaks(infected)
    peaks = np.setdiff1d(all_peaks, low_prom)
    return peaks


def train_peaks(rw):
    data = []
    model = ReinfectionModel(rw=rw)
    for sw in range(15, 365, 5):
        model.sw = 365.0 / sw
        model_out = model.train()
        infected = diagnosed_infected_per_1000(model_out)
        peaks = get_peaks(infected)

        std_infected = np.std(infected)
        mean_infected = np.mean(infected)
        mean_peak_t = np.mean(np.diff(model.t[peaks] * 12))
        data.append([rw, sw, mean_infected, std_infected, mean_peak_t])

    return data


def rw_sw_combinations():
    with multiprocessing.Pool() as pool:
        data = np.asarray(pool.map(train_peaks, np.arange(10, 365, 5)))

    df = pd.DataFrame(
        data.reshape(data.shape[0] * data.shape[1], 5),
        columns=["rw", "sw", "mean_infected", "std_infected", "t"],
    )
    
    df["rw"] = df["rw"].astype(int)
    df["sw"] = df["sw"].astype(int)
    return df


def plot_heatmap():
    df = rw_sw_combinations()
    sns.heatmap(df.pivot(index="rw", columns="sw", values="t"), cmap="magma")
    plt.savefig("t.png", dpi=300, transparent=True, bbox_inches='tight')
    plt.clf()

    sns.heatmap(df.pivot(index="rw", columns="sw", values="mean_infected"), cmap="magma")
    plt.savefig("mean_infected.png", dpi=300, transparent=True, bbox_inches='tight')
    plt.clf()

    sns.heatmap(df.pivot(index="rw", columns="sw", values="std_infected"), cmap="magma")
    plt.savefig("std_infected.png", dpi=300, transparent=True, bbox_inches='tight')
    plt.clf()

if __name__ == "__main__":
    print("Fix rw")
    # Plot sw @ rw = 30
    plot_infected(0, 0, label="Baseline (Aware)", linestyle="--", color=colors[0])

    for j, sw in enumerate([15, 30, 60, 120, 240, 365]):
        print("sw =", sw)
        plot_infected(rw=30, sw=sw, label=sw, color=colors[j + 1])

    # plt.xlim([0, 40])
    plt.grid(color="0.9", linestyle="-", linewidth=1)
    legend = plt.legend()
    frame = legend.get_frame()
    frame.set_facecolor("1.0")
    frame.set_edgecolor("1.0")

    plt.xlabel("t in Monaten")
    plt.ylabel("Infizierte pro 1000 Individuen")
    plt.savefig("increment_sw.pdf", dpi=300)
    plt.show()
    plt.clf()

    print("#######################################")
    print("Fix sw")


    # Plot rw @ sw = 30
    plot_infected(0, 0, label="Baseline (Aware)", linestyle="--", color=colors[0])
    for j, rw in enumerate([15, 30, 60, 120, 240, 365]):
        print("rw =", rw)
        plot_infected(rw=rw, sw=30, label=rw, color=colors[j + 1])

    # plt.xlim([0, 40])
    plt.grid(color="0.9", linestyle="-", linewidth=1)
    legend = plt.legend()
    frame = legend.get_frame()
    frame.set_facecolor("1.0")
    frame.set_edgecolor("1.0")

    plt.xlabel("t in Monaten")
    plt.ylabel("Infizierte pro 1000 Individuen")
    plt.savefig("increment_rw.pdf", dpi=300)
    plt.show()
    plt.clf()
    
    ###
    print("Plot heatmaps")
    plot_heatmap()
