import numpy as np
from transmission_model import TransmissionModel

class ReinfectionModel(TransmissionModel):
    def __init__(self, rw=120.0, sw=120.0, **kwargs):
        super().__init__(**kwargs)

        try:
            self.rw = 365.0 / rw
        except ZeroDivisionError:
            self.rw = 0

        try:
            self.sw = 365.0 / sw
        except ZeroDivisionError:
            self.sw = 0

    def model_labels(self):
        return super().model_labels() + ("SW",)

    def model_args(self):
        return super().model_args() + (self.rw, self.sw)

    def init_model_parameters(self):
        return np.append(super().init_model_parameters(), [0.0])  # append SW init value

    def model(
        self,
        odes,
        t,
        intervention,
        start_t,
        stop_t,
        a,
        beta,
        u_1,
        u_2,
        n,
        na,
        v,
        va,
        y_1,
        y_2,
        y_3,
        k,
        p,
        d,
        sigma,
        r1,
        r2,
        r3,
        r4,
        rw,
        sw,
    ):
        S = odes[0]
        EE = odes[1]
        IM = odes[2]
        IS = odes[3]
        IQ = odes[4]
        IQa = odes[5]
        R = odes[6]
        RQ = odes[7]
        Sa = odes[8]
        EEa = odes[9]
        IMa = odes[10]
        ISa = odes[11]
        DD = odes[12]
        DDQ = odes[13]
        DDQa = odes[14]
        RM = odes[15]
        RMa = odes[16]
        Sw = odes[17]

        NN = S + EE + IM + IS + IQ + IQa + R + Sa + EEa + IMa + ISa + Sw

        λ_aware = d * (IQ + IQa)

        trans_coef = beta / (NN - IQ - IQa)
        individual_coef = beta / (
            S + EE + IM + IS + RQ + Sw + RM + r3 * (Sa + EEa + IMa + ISa + RMa)
        )

        if (
            t >= start_t and t <= stop_t + start_t
        ):  # Impose gov measures when in given time frame
            gov_coef = r4
        else:
            gov_coef = 1
        vec_inf = np.array([IM, IS, IMa, ISa])

        # compute awareness dependent measurement lambdas
        λ, λa = self.get_lambdas(
            intervention,
            sigma,
            r1,
            r2,
            r3,
            trans_coef,
            individual_coef,
            gov_coef,
            vec_inf,
        )

        d_S_dt = Sa * u_1 - k * S * λ_aware - S * λ + sw * Sw
        d_Sa_dt = -Sa * u_1 + k * S * λ_aware - Sa * λa

        d_EE_dt = -a * EE + EEa * u_1 - k * EE * λ_aware + S * λ
        d_EEa_dt = -a * EEa - EEa * u_1 + k * EE * λ_aware + Sa * λa

        d_IM_dt = p * a * EE - IM * y_1 + IMa * u_1 - k * IM * λ_aware
        d_IS_dt = (1 - p) * a * EE - v * IS + ISa * u_2 - IS * λ_aware
        d_IQ_dt = -n * IQ + v * IS - IQ * y_2
        d_IQa_dt = va * ISa - IQa * y_3 - IQa * na
        d_IMa_dt = p * a * EEa - IMa * y_1 - IMa * u_1 + k * IM * λ_aware
        d_ISa_dt = (1 - p) * a * EEa - va * ISa - ISa * u_2 + IS * λ_aware

        d_R_dt = IM * y_1 + IMa * y_1 + IQ * y_2 + IQa * y_3
        d_RQ_dt = IQ * y_2 + IQa * y_3 - rw * RQ
        d_RM_dt = IM * y_1 + RMa * u_1 - k * RM * λ_aware - rw * RM
        d_RMa_dt = IMa * y_1 - RMa * u_1 + k * RM * λ_aware

        d_DD_dt = n * IQ + IQa * na
        d_DDQ_dt = n * IQ
        d_DDQa_dt = IQa * na

        d_Sw_dt = rw * R - sw * Sw

        return (
            d_S_dt,
            d_EE_dt,
            d_IM_dt,
            d_IS_dt,
            d_IQ_dt,
            d_IQa_dt,
            d_R_dt,
            d_RQ_dt,
            d_Sa_dt,
            d_EEa_dt,
            d_IMa_dt,
            d_ISa_dt,
            d_DD_dt,
            d_DDQ_dt,
            d_DDQa_dt,
            d_RM_dt,
            d_RMa_dt,
            d_Sw_dt,
        )