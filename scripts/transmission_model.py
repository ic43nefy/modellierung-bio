import numpy as np
import pandas as pd
from scipy.integrate import odeint


class TransmissionModel:
    def __init__(
        self,
        intervention="Baseline",
        k=0.5,
        u1=365 / 30.0,
        u2=365 / 60.0,
        d=1,
        va=365 / 3.0,
        t_start=0.0,
    ):
        self.intervention = intervention
        self.c = 13.85 * 365
        self.sigma = 0.5
        self.a = 365 / 4.0
        self.p = 0.82
        self.y_1 = 365 / 7.0
        self.y_2 = 365 / 14.0
        self.y_3 = 365 / 12.0
        self.v = 365 / 5.0
        self.f = 0.016
        self.n = self.y_2 * self.f / (1 - self.f)
        self.r0 = 2.5
        self.eps = 0.0478794
        self.beta = self.c * self.eps
        self.k = k
        self.d = d
        self.u_1 = u1
        self.u_2 = u2
        self.va = va
        self.na = 0.307239
        self.r1 = 1.0
        self.r2 = 1.0
        self.r3 = 1.0
        self.r4 = 1.0
        self.stop_t = 3 / 12.0
        self.start_t_baseline = 0.1037
        self.t_start = t_start
        self.t_end = 10.0
        self.t_resolution = 20
        self.N_tot = 17 * 10**6
        self.inf_init = 1
        self.t = np.linspace(
            self.t_start,
            self.t_end,
            int(365 * (self.t_end - self.t_start) * self.t_resolution),
        )

    def set_baseline(self):
        """Set model to baseline model without disease awareness by setting according parameters to zero."""
        self.k = 0
        self.u1 = 0
        self.u2 = 0
        self.d = 0
        self.va = 0
        self.t_start = 0

    def model(
        self,
        odes,
        t,
        intervention,
        start_t,
        stop_t,
        a,
        beta,
        u_1,
        u_2,
        n,
        na,
        v,
        va,
        y_1,
        y_2,
        y_3,
        k,
        p,
        d,
        sigma,
        r1,
        r2,
        r3,
        r4,
    ):
        S, EE, IM, IS, IQ, IQa, R, RQ, Sa, EEa, IMa, ISa, DD, DDQ, DDQa, RM, RMa = odes
        NN = S + EE + IM + IS + IQ + IQa + R + Sa + EEa + IMa + ISa

        λ_aware = d * (IQ + IQa)

        trans_coef = beta / (NN - IQ - IQa)
        individual_coef = beta / (
            S + EE + IM + IS + RQ + RM + r3 * (Sa + EEa + IMa + ISa + RMa)
        )

        gov_coef = 1
        if (
            t >= start_t and t <= stop_t + start_t
        ):  # Impose gov when in given time frame
            gov_coef = r4
        vec_inf = np.array([IM, IS, IMa, ISa])

        λ_inter, λa_inter = self.get_lambdas(
            intervention,
            sigma,
            r1,
            r2,
            r3,
            trans_coef,
            individual_coef,
            gov_coef,
            vec_inf,
        )

        d_S_dt = Sa * u_1 - k * S * λ_aware - S * λ_inter
        d_Sa_dt = -Sa * u_1 + k * S * λ_aware - Sa * λa_inter

        d_EE_dt = -a * EE + EEa * u_1 - k * EE * λ_aware + S * λ_inter
        d_IM_dt = p * a * EE - IM * y_1 + IMa * u_1 - k * IM * λ_aware
        d_IS_dt = (1 - p) * a * EE - v * IS + ISa * u_2 - IS * λ_aware
        d_IQ_dt = -n * IQ + v * IS - IQ * y_2
        d_IQa_dt = va * ISa - IQa * y_3 - IQa * na
        d_R_dt = IM * y_1 + IMa * y_1 + IQ * y_2 + IQa * y_3
        d_EEa_dt = -a * EEa - EEa * u_1 + k * EE * λ_aware + Sa * λa_inter
        d_IMa_dt = p * a * EEa - IMa * y_1 - IMa * u_1 + k * IM * λ_aware
        d_ISa_dt = (1 - p) * a * EEa - va * ISa - ISa * u_2 + IS * λ_aware
        d_DD_dt = n * IQ + IQa * na
        d_DDQ_dt = n * IQ
        d_DDQa_dt = IQa * na
        d_RQ_dt = IQ * y_2 + IQa * y_3

        d_RM_dt = IM * y_1 + RMa * u_1 - k * RM * λ_aware
        d_RMa_dt = IMa * y_1 - RMa * u_1 + k * RM * λ_aware

        return (
            d_S_dt,
            d_EE_dt,
            d_IM_dt,
            d_IS_dt,
            d_IQ_dt,
            d_IQa_dt,
            d_R_dt,
            d_RQ_dt,
            d_Sa_dt,
            d_EEa_dt,
            d_IMa_dt,
            d_ISa_dt,
            d_DD_dt,
            d_DDQ_dt,
            d_DDQa_dt,
            d_RM_dt,
            d_RMa_dt,
        )

    def get_lambdas(
        self,
        intervention,
        sigma,
        r1,
        r2,
        r3,
        trans_coef,
        individual_coef,
        gov_coef,
        vec_inf,
    ):
        # Define transmission matrices for different interventions
        tr_mat = {
            "Baseline": trans_coef
            * np.array([[sigma, 1.0, sigma, 1.0], [sigma, 1.0, sigma, 1.0]]),
            "Mask": trans_coef
            * np.array([[sigma, 1.0, r1 * sigma, r1], [sigma, 1.0, r1 * sigma, r1]]),
            "Hand": trans_coef
            * np.array([[sigma, 1.0, sigma, 1.0], [r2 * sigma, r2, r2 * sigma, r2]]),
            "Individual": individual_coef
            * np.array(
                [
                    [sigma, 1.0, r3 * sigma, r3],
                    [r3 * sigma, r3, r3**2 * sigma, r3**2],
                ]
            ),
            "Gov": gov_coef
            * np.array([[sigma, 1.0, sigma, 1.0], [r2 * sigma, r2, r2 * sigma, r2]]),
        }

        # Compute λ_inter and λa_inter for the given intervention and infected vector vec_inf
        tr_dot = np.dot(tr_mat[intervention], vec_inf)
        return tr_dot[0], tr_dot[1]

    def init_model_parameters(self):
        model_init = np.zeros(17)
        model_init[0] = self.N_tot - self.inf_init  # Total population without infected
        model_init[3] = self.inf_init  # First infected

        return model_init

    def model_args(self):
        return (
            self.intervention,
            self.t_start,
            self.stop_t,
            self.a,
            self.beta,
            self.u_1,
            self.u_2,
            self.n,
            self.na,
            self.v,
            self.va,
            self.y_1,
            self.y_2,
            self.y_3,
            self.k,
            self.p,
            self.d,
            self.sigma,
            self.r1,
            self.r2,
            self.r3,
            self.r4,
        )

    def model_labels(self):
        return (
            "S",
            "EE",
            "IM",
            "IS",
            "IQ",
            "IQa",
            "R",
            "RQ",
            "Sa",
            "EEa",
            "IMa",
            "ISa",
            "DD",
            "DDQ",
            "DDQa",
            "RM",
            "RMa",
        )

    def train(self):
        """
        Trains the model and returns a Pandas DataFrame containing the simulation results.
        """
        # Run simulation
        model_out = odeint(
            self.model,
            self.init_model_parameters(),
            self.t,
            args=self.model_args(),
        )

        model_out = pd.DataFrame(model_out, columns=self.model_labels())
        model_out["t"] = self.t
        model_out["NN"] = model_out[
            ["S", "EE", "IM", "IS", "IQ", "IQa", "R", "Sa", "EEa", "IMa", "ISa"]
        ].sum(axis=1)
        return model_out
