% Created 2023-04-26 Wed 17:05
% Intended LaTeX compiler: pdflatex
\documentclass[a4paper,12pt, dvipsnames]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[sc]{mathpazo}
\usepackage{pdfpages}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{svg}
\usepackage{tikz}
\usepackage{xcolor}
\usepackage{caption}
\usepackage{subcaption}
\author{Ivan Cygankov}
\date{\today}
\title{Belegarbeit}
\hypersetup{
 pdfauthor={Ivan Cygankov},
 pdftitle={Belegarbeit},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 28.2 (Org mode 9.6.1)}, 
 pdflang={German}}
\usepackage{calc}
\newlength{\cslhangindent}
\setlength{\cslhangindent}{1.5em}
\newlength{\csllabelsep}
\setlength{\csllabelsep}{0.6em}
\newlength{\csllabelwidth}
\setlength{\csllabelwidth}{0.45em * 3}
\newenvironment{cslbibliography}[2] % 1st arg. is hanging-indent, 2nd entry spacing.
 {% By default, paragraphs are not indented.
  \setlength{\parindent}{0pt}
  % Hanging indent is turned on when first argument is 1.
  \ifodd #1
  \let\oldpar\par
  \def\par{\hangindent=\cslhangindent\oldpar}
  \fi
  % Set entry spacing based on the second argument.
  \setlength{\parskip}{\parskip +  #2\baselineskip}
 }%
 {}
\newcommand{\cslblock}[1]{#1\hfill\break}
\newcommand{\cslleftmargin}[1]{\parbox[t]{\csllabelsep + \csllabelwidth}{#1}}
\newcommand{\cslrightinline}[1]
  {\parbox[t]{\linewidth - \csllabelsep - \csllabelwidth}{#1}\break}
\newcommand{\cslindent}[1]{\hspace{\cslhangindent}#1}
\newcommand{\cslbibitem}[2]
  {\leavevmode\vadjust pre{\hypertarget{citeproc_bib_item_#1}{}}#2}
\makeatletter
\newcommand{\cslcitation}[2]
 {\protect\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother\begin{document}

\maketitle

\section{Einleitung}
\label{sec:org514e430}

Im Jahr 2019 brach die COVID-19-Pandemie aus, die bis heute dazu geführt hat, dass sich 762 Millionen Menschen mit SARS-CoV-2 infiziert haben und sechs Millionen Todesfälle zu verzeichnen sind\footnote{\url{https://covid19.who.int/}}. Die Ausbreitung des Virus hat verschiedenste Maßnahmen, wie bspw. Lockdowns, zur Folge gehabt. Infolgedessen haben Forschende untersucht, wie sich Maßnahmen auf den Verlauf der Pandemie auswirken können.

So haben bspw. Güner et al. die Wirksamkeit von verschiedenen Interventionsmaßnahmen zur Bekämpfung von COVID-19 in der Türkei untersucht. Dazu haben sie ein Modell zur Übertragungsdynamik von COVID-19 entwickelt und dieses Modell genutzt, um verschiedene Szenarien zu simulieren. Die Ergebnisse zeigen, dass eine Kombination aus Kontaktbeschränkungen, Schulschließungen und anderen Maßnahmen am effektivsten bei der Eindämmung der Pandemie war \cslcitation{1}{[1]}.
Talic et al. entwickelten ein Modell zur Übertragungsdynamik von COVID-19, das die Auswirkungen von verschiedenen Maßnahmen zur Bekämpfung der Pandemie auf die Sterblichkeitsrate und die Infektionsrate untersucht. Die Ergebnisse zeigen insbesondere, dass Kontaktbeschränkungen, Masketragen und die Impfpflicht zur Eindämmung einer Pandemie beitragen. Außerdem zeigen die Ergebnisse, dass die Sterblichkeitsrate durch frühzeitige Interventionen signifikant reduziert werden kann \cslcitation{2}{[2]}.

Das Ziel dieser Belegarbeit war die Implementierung und Evaluierung eines "`Reinfektionsmodells"', welches auf dem Übertragungsmodell mit \emph{Awareness}-Dynamik von Teslya et al. basiert.

\section{Zusammenfassung des Papers von Teslya et al.}
\label{sec:org284d965}

Teslya et. al untersuchten die Auswirkungen verschiedener Maßnahmen auf das Infektionsgeschehen der COVID-19-Pandemie mithilfe eines weiterentwickelten SEIR-Modells \cslcitation{3}{[3]}.
Anders als beim SEIR-Modell unterscheiden Teslya et al. die Infizierten zwischen Infizierten mit einem milden Krankheitsverlauf und Infizierten mit einem schweren Krankheitsverlauf.
Hierbei trafen sie die Annahme, dass alle Erkrankten mit schweren Symptomen diagnostiziert und isoliert werden. Ein Anteil der diagnostizierten Individuen mit schwerem Krankheitsverlauf verstirbt und ein Anteil wird wieder genesen. Individuen mit milden Symptomen werden nicht mit COVID-19 diagnostiziert und begeben sich somit nicht in Isolation. Außerdem verstirbt kein Individuum mit milden Krankheitsverlauf, sodass alle diese Individuen vollständig gesund werden.

In der Arbeit wurden vier Maßnahmen untersucht: das Händewaschen, das Tragen von Masken, das selbst auferlegte \emph{Social Distancing} (dt. "`räumliche Distanzierung"') und das von einer Regierung verordnete \emph{Social Distancing}.
Diese wurden gegen ein Baseline-Modell verglichen, bei dem die Effektivität der eben genannten Maßnahmen auf Null gesetzt wurden. Die Effektivität einer Maßnahme wird bspw. anhand der Abnahme von Infektionszahlen oder einer zeitlichen Verzögerung der Pandemie bemessen.
Um Maßnahmen gezielt in das Modell einarbeiten zu können entwickelten Teslya et al. den Begriff der \emph{disease awareness} (dt. "`Krankheitsbewusstsein"'). Dieser beschreibt, dass die Effektivität einer Maßnahme vom Krankheitsbewusstsein abhängig ist.
Teslya et al. nahmen an, dass Maßnahmen effektiver sind, wenn Individuen krankheitsbewusster sind, da sie sich der Gefahr einer Infektion und etwaigen Folgen bewusster sind.

Die Studie ergab, dass das Zusammenwirken der oben genannten Maßnahmen dazu beitragen kann, die Verbreitung der Pandemie effektiv einzudämmen. Obwohl ein Lockdown dazu beitragen kann, den Höhepunkt der Pandemie zu verzögern, führt er ohne zusätzliche Maßnahmen nicht zu einer Reduzierung der Gesamtzahl der Fälle. Diese Verzögerung kann jedoch vorteilhaft sein, da in dieser Zeit beispielsweise die Entwicklung eines Impfstoffs möglich ist. Es wurde auch festgestellt, dass eine zu späte Einführung von Lockdowns und anderen Maßnahmen deren Wirksamkeit verringert. Die Autoren der Studie stellten fest, dass ein hohes Krankheitsbewusstsein dazu beitragen kann, die Wirksamkeit der Maßnahmen zu erhöhen und somit die Pandemie einzudämmen.

\section{Eigener Anteil}
\label{sec:orgd68cccf}

\subsection{Implementierung des Übertragunsmodells}
\label{sec:org9f4649f}

Teslya et al. implementierten das Modell mit dem Software-System \emph{Wolfram Mathematica}\footnote{Github-Repository von Teslya et al.: \url{https://github.com/lynxgav/COVID19-mitigation}}.
Die in dieser Arbeit ausgewählte Programmiersprache war \emph{Python (Version 3.11)}.
Dafür wurde das Modul \emph{odeint} der Programmbibliothek \emph{scipy} verwendet, mit welcher man Differentialgleichungen erster Ordnung lösen kann \cslcitation{4}{[4]}.
Die Implementation des Übertragungsmodells befindet sich in der beiliegenden Datei \emph{transmission\textsubscript{model.py}}, das Modell aus dieser Arbeit befindet sich in \emph{reinfecion\textsubscript{model.py}}.
Teslya et al. haben verschiedene Funktionen zur Analyse des \emph{Awareness}-Modells implementiert, welche in dieser Belegarbeit nachimplementiert wurden (siehe Datei \emph{evaluation.py}).
\texttt{attack\_rate} berechnet den Anteil von verstorbenen Individuen und genesenen Individuen mit schwerem Krankheitsverlauf zur Gesamtbevölkerung. Mit der Funktion \texttt{diagnosed\_infected} berechnet man die Anzahl der mit COVID-19 diagnostizierten Individuen.
Die Funktion \texttt{t\_to\_peak} gibt den Zeitpunkt \(t\) zurück, an dem der maximale Wert diagnostizierter Individuen erreicht wurde.
Damit ist es einerseits möglich die Ergebnisse der Arbeit zu reproduzieren und zu validieren und andererseits die Ergebnisse dieser Arbeit mit der Arbeit von Teslya et. al zu vergleichen.

\subsection{Reinfektionsmodell}
\label{sec:org14cceb4}

Um die Reinfektion bereits infizierter Individuen zu modellieren, wurde das Übertragungsmodell erweitert. Dazu wurden zwei neue Compartments, nämlich \(S_0\) und \(S_1\), eingeführt, wobei \(S_0\) im Wesentlichen mit dem \(S\) Compartment aus dem Originalmodell übereinstimmt. Das Modell beginnt mit einer vollständig gesunden Population im \(S_0\) Compartment. Diese Individuen werden mit einer Rate von \(\lambda\) latent infiziert und landen im \(E\) Compartment. Anschließend werden sie entweder mit einem schweren Verlauf (\(I_Q\) Compartment) oder einem milden Verlauf (\(I_M\) Compartment) infiziert. Nach überstandener Krankheit wechseln infizierte Individuen in ihre jeweiligen Genesungs-Compartments \(R_Q\) (genesen, schwerer Krankheitsverlauf) oder \(R_M\) (genesen, milder Krankheitsverlauf). Individuen in einem der beiden \(R\) Compartments können sich zunächst nicht erneut infizieren, da sie immun sind.

Das Modell wurde um die Reinfektion erweitert, indem Individuen in den \(R_Q\) und \(R_M\) Compartments jeweils mit einer Rate \(r_w\) in das \(S_1\) Compartment wechseln können, was den abnehmenden Schutz vor COVID-19 nach überstandener Infektion modelliert. Individuen im \(S_1\) Compartment können sich zunächst nicht mit der noch dominierenden Variante infizieren, da Individuen erst im \(S_0\) Compartment dafür anfällig sind. Mit einer konstanten Rate \(s_w\) verlieren Individuen im \(S_1\) Compartment ihren Schutz vor einer Infektion und kehren zum ursprünglichen \(S_0\) Compartment zurück. Dadurch können Individuen in \(S_1\) nicht sofort erneut infiziert werden, was es ermöglicht, verschiedene COVID-19-Varianten zu modellieren. Eine vereinfachte Darstellung des erweiterten Modells finden Sie in Abbildung \ref{fig:trans_model}. Eine vollständige Liste aller Differentialgleichungen finden Sie im Anhang der Arbeit von Teslya et al. \cslcitation{3}{[3]}. Die Gleichungen (1)-(4) sind die erweiterten Gleichungen in dieser Arbeit, wobei die farblich hervorgehobenen Teile den ursprünglichen Gleichungen hinzugefügt wurden. Im folgenden wird das in diesem Abschnitt vorgestellte Modell \emph{Reinfektionsmodell} genannt.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\textwidth]{images/trans_model.png}
\caption{\label{fig:trans_model}Vereinfachte Darstellung des Reinfektionsmodell.}
\end{figure}

\begin{align}
  S_0 &= \mu_1 S_a &&- k\lambda_{aware} S_0 - \lambda_{inf} S &&+ \color{ForestGreen}s_w S_1\\
  R_Q &= \gamma_2 I_Q &&+ \gamma_3 I_{Qa} &&- \color{ForestGreen}r_w R_Q\\
  R_M &= \gamma_1 I_M &&+ \mu_1 R_{Ma} - k \lambda_{aware} R_M &&- \color{ForestGreen}r_w R_M\\
  \color{ForestGreen}S_1 &= \color{ForestGreen}r_wR_Q  &&+ \color{ForestGreen}r_wR_M &&- \color{ForestGreen}s_wS_1
\end{align}

\subsection{Parameter}
\label{sec:orgd99cae2}

Die Parameterwahl \(r_w\) und \(s_w\) basiert auf verschiedenen Studien zur Reinfektion mit COVID-19, die die Tage bis zu einer erneuten Infektion untersucht haben. 
Eine Zusammenfassung dieser Studien ist in Tabelle \ref{tab:reinfections} aufgeführt. 
Basierend auf dem Durchschnitt der Studienergebnisse ergibt sich, dass eine Person im Durchschnitt alle 320 Tage erneut mit COVID-19 infiziert wird. 
Daher müssen die Parameter \(r_w\) und \(s_w\) so gewählt werden, dass eine Person, die in das \(R\) Compartment wechselt, nach 320 Tagen erneut in das \(I\) Compartment wechselt. 
Teslya et al. haben eine Verzögerung von 4 Tagen zwischen Infektion und Ausbruch der Krankheit modelliert (Parameter \(\alpha\)). 
Insgesamt bleiben also 316 Tage übrig, in denen sich Individuen in den Compartments \(R_M\), \(R_Q\) und \(S_1\) aufhalten können, bevor sie sich erneut infizieren.


\begin{table}[h]
\centering
\begin{tabular}{lr}
Studie & Tage bis zur Reinfektion\\[0pt]
\hline
Malhotra et al. \cslcitation{5}{[5]} & 180\\[0pt]
Eythorsson et al. \cslcitation{6}{[6]} & 287\\[0pt]
Guedes et al.     \cslcitation{7}{[7]} & 429\\[0pt]
Medić et al. \cslcitation{8}{[8]} & 340\\[0pt]
Flacco et al. \cslcitation{9}{[9]} & > 365\\[0pt]
\hline
\hline
Durchschnitt & 320\\[0pt]
\end{tabular}
\caption{\label{tab:reinfections}Tage bis zu einer Reinfektion mit COVID-19 aus verschiedenen Studien.}

\end{table}


\section{Ergebnisse und Diskussion}
\label{sec:org9674a88}


Um die Validität des Reinfektionsmodells zu prüfen, wurden die Parameter \(r_w\) und \(s_w\) variiert und die daraus resultierenden Modelle mit den Übertragungsmodellen von Teslya et al. verglichen. Hierbei kamen die Funktionen aus Abschnitt \ref{sec:org9f4649f} zum Einsatz. Es erfolgte eine Untersuchung, ob sich das Verhalten des Reinfektionsmodells analog zum Übertragungsmodell verhält, wenn die Parameter \(r_w\) und \(s_w\) auf Null gesetzt werden. Darüber hinaus wurde geprüft, ob die Einführung der Parameter eine Zunahme der Infektionszahlen zur Folge hat und ob die Zeit zwischen den Spitzen der Infektionszahlen etwa der Reinfektionszeit von 320 Tagen entspricht. Zur Veranschaulichung, dass das Reinfektionsmodell die ursprüngliche Awareness-Dynamik beibehält, wurden ausgewählte Ergebnisse von Teslya et al. nachgestellt.

\subsection{Vergleich zwischen Übertragungsmodell und Reinfektionsmodell}
\label{sec:org04aa668}

Um die Validität des Modells zu testen wurde getestet, ob das Reinfektionsmodell gleiche Ausgaben liefert, wenn die eingeführten Parameter \(r_w\) und \(s_w\) auf Null gesetzt worden sind.
Dafür wurden die Funktionen aus \ref{sec:org9f4649f} verwendet.
Bereits Teslya et al. verwendeten eine ähnliche Methodik, um ihr \emph{Baseline}-Übertragungsmodell, mit dem um das Krankheitsbewusstsein erweiterte Übertragungsmodell zu vergleichen und zu validieren.
Die Analyse dieser Ausarbeitung ergab, dass sich das Reinfektionsmodell erwartungsgemäß verhielt und dieselben Ausgaben liefert. Der Quellcode befindet sich in der beiliegenden \texttt{evaluation.py}.

\subsection{Untersuchung der Parameter des Reinfektionsmodells}
\label{sec:org7375c87}

\begin{figure}[h!]
   \centering
   \begin{subfigure}[b]{0.75\textwidth}
        \includegraphics[width=\textwidth] {./images/increment_sw.pdf}
        \caption{Festes $r_w$ = 30 bei variablen $s_w$.}
   \end{subfigure}
   \begin{subfigure}[b]{0.75\textwidth}
        \includegraphics[width=\textwidth] {./images/increment_rw.pdf}
        \caption{Festes $s_w$ = 30 bei variablen $r_w$.}
   \end{subfigure}
   \caption{Vergleich zwischen dem Übertragungsmodell und dem Reinfektionsmodell für verschiedene Parametereinstellungen}
\label{fig:comparison}
\end{figure}

Um die Auswirkungen auf den Verlauf der Pandemie zu untersuchen, wurden die Parameter \(r_w\) und \(s_w\) des Reinfektionsmodells variiert. Die Dynamik und Entwicklung der Pandemie wurde anhand der Infektionszahlen pro 1000 Individuen gemessen. Die Auswirkungen der Parameter wurden untersucht, indem einer der Parameter auf einen festen Wert gesetzt und der jeweils andere variiert wurde. Die Untersuchungsdauer betrug 10 Jahre bzw. 120 Monate.

Eine Testreihe wurde durchgeführt, bei der \(r_w\) auf einen festen Wert von 30 gesetzt wurde und \(s_w\) variiert wurde, während eine andere Testreihe mit einem festen \(s_w\) und variierenden \(r_w\) durchgeführt wurde. Die Ergebnisse zeigten, dass sich \(r_w\) und \(s_w\) unterschiedlich auf den Pandemie-Verlauf auswirkten.

Beim Vergleich der grünen Kurven in Abbildung 2a für \(r_w = 30\) und \(s_w = 120\) mit der grünen Kurve für \(s_w = 30\) und \(r_w = 120\) fällt auf, dass die Kurve mit einem festen, kleinen \(s_w\) und großem \(r_w\) deutlich gestauchter ist als die Kurve mit einem festen, kleinen \(r_w\) und großem \(s_w\). Der erste Peak der Kurve mit dem festen \(r_w\) liegt bei 21,6 Monaten, während er sich bei dem festen \(s_w\) bei 17,4 Monaten befindet. Dieser Effekt ist bei allen Kurven zu beobachten.

Des Weiteren zeigt sich, dass die Wellendynamik für große \(r_w\) schneller abnimmt und in einen scheinbar stabilen Zustand übergeht, während die Infektionszahlen für \(s_w\) nur für kleine Ausprägungen stabil bleiben. Dies deutet darauf hin, dass \(r_w\) einen stärkeren Einfluss auf die Wellendynamik hat.
saddssadsdadsa
\begin{figure}[h!]
   \centering
   \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth] {./images/mean_infected.png}
        \caption{Durchschnittliche Zahl infizierter Individuen.}
   \end{subfigure}
   \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth] {./images/std_infected.png}
        \caption{Standardabweichung infizierter Individuen.}
   \end{subfigure}
   \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth] {./images/t.png}
        \caption{Durchschnittliche Zeit in Monaten zwischen den Spitzenwerten.}
   \end{subfigure}
   \caption{Vergleich zwischen dem Übertragungsmodell und dem Reinfektionsmodell für verschiedene Parametereinstellungen von $r_w$ und $s_w$ als Heatmap bezüglich infizierter Individuen pro 1000.}
\label{fig:peaks}
\end{figure}

Zusätzlich wurden grundlegende statistische Untersuchungen hinsichtlich der Parameter \(r_w\) und \(s_w\) vorgenommen, bei denen nun beide Parameter variiert wurden (siehe Abbildung 3). Die Untersuchungen erfolgten über den gesamten Zeitraum der Pandemie (10 Jahre).
Es wurden die mittleren Infektionszahlen (Abbildung 3a), die Standardabweichung (Abbildung 3b), sowie die durchschnittliche Zeit zwischen den Peaks der Infektionszahlen (Abbildung 3c) analysiert.

Die Ergebnisse zeigen, dass kleine \(r_w\) und \(s_w\) zu höheren mittleren Infektionszahlen führen, wie in Abbildung 3a dargestellt. Insbesondere ist in der oberen linken Ecke der Heatmap im Bereich \(r_w \in [15, 75]\) und \(s_w \in [15, 60]\) eine deutlich höhere Infektionsrate zu beobachten, die mit steigender Parameterzahl abnimmt.

Die Analyse der Standardabweichung in Abbildung 3b) ergab jedoch, dass geringe \(r_w\) und \(s_w\) geringere Abweichungen vom Mittelwert aufweisen als große \(r_w\) und \(s_w\). Dieses Ergebnis ist überraschend, da es im Widerspruch zu den Erwartungen steht, die auf der Basis der Abbildung 3a) formuliert wurden. Dieses Phänomen kann durch Abbildung 2 erklärt werden.
Betrachtet man die orangene Kurve (\(r_w = 30, s_w = 30\)) in den Abbildungen 2a und 2b so sieht man, dass bei etwa \(t = 27\) ein vermeintlich stabiler Zustand des Reinfektionsmodells hinsichtlich der Infektionszahlen entsteht. Die Standardabweichung für \(t > 27\) ist sehr gering und dies spiegelt sich in Abbildung 3b wieder.

Abbildung 3c beschreibt die mittlere Zeit zwischen den Spitzenwerten der Infektionszahlen. Hier sieht man, dass \(r_w\) und \(s_w\) gleichermaßen Einfluss auf den Abstand zwischen den Infektionszahlen haben. Mit ansteigenden \(s_w\) und \(r_w\) steigt auch die Zeit zwischen den Spitzenwerten an.


\section{Zusammenfassung}
\label{sec:org629002d}

Zusammenfassend lässt sich sagen, dass die vorliegende Arbeit ein erweitertes Reinfektionsmodell auf Basis des Teslya et al. Übertragungsmodells präsentiert. Obwohl beide Modelle Toy-Modelle sind und einige Aspekte der COVID-19-Pandemie nicht berücksichtigen, tragen sie dennoch zum tieferen Verständnis der Pandemie bei und zeigen, dass präventive Maßnahmen wie Händewaschen und soziale Distanzierung auch bei einer Reinfektion wirksam sein können.

Die in dieser Arbeit vorgenommenen Erweiterungen erlauben es, die Dynamik von Infektionswellen besser zu simulieren und ihre Auswirkungen zu untersuchen. Es ist wichtig zu betonen, dass diese Modelle keine genaue Vorhersage der tatsächlichen Ausbreitung der Pandemie machen können, da sie einige wichtige Aspekte wie die Auswirkungen von Impfungen oder die Entstehung neuer Virusvarianten nicht berücksichtigen.
Trotzdem sind diese Modelle wertvolle Werkzeuge, um verschiedene Szenarien zu simulieren und präventive Maßnahmen zu evaluieren.

\clearpage
\thispagestyle{empty}
\section{Literatur}
\label{sec:org78e5d08}

\begin{cslbibliography}{0}{0}
\cslbibitem{1}{\cslleftmargin{[1]}\cslrightinline{H. GÜNER, \. HASANO\ GLU, and F. AKTAŞ, “COVID-19: Prevention and control measures in community,” \textit{Turkish journal of medical sciences}, vol. 50, no. 9, pp. 571–577, Jan. 2020, doi: \href{https://doi.org/10.3906/sag-2004-146}{10.3906/sag-2004-146}.}}

\cslbibitem{2}{\cslleftmargin{[2]}\cslrightinline{S. Talic \textit{et al.}, “Effectiveness of public health measures in reducing the incidence of covid-19, SARS-CoV-2 transmission, and covid-19 mortality: Systematic review and meta-analysis,” \textit{Bmj}, vol. 375, p. e068302, Nov. 2021, doi: \href{https://doi.org/10.1136/bmj-2021-068302}{10.1136/bmj-2021-068302}.}}

\cslbibitem{3}{\cslleftmargin{[3]}\cslrightinline{A. Teslya, T. M. Pham, N. G. Godijk, M. E. Kretzschmar, M. C. J. Bootsma, and G. Rozhnova, “Impact of self-imposed prevention measures and short-term government-imposed social distancing on mitigating and delaying a COVID-19 epidemic: A modelling study,” \textit{Plos medicine}, vol. 17, no. 7, p. e1003166, Jul. 2020, doi: \href{https://doi.org/10.1371/journal.pmed.1003166}{10.1371/journal.pmed.1003166}.}}

\cslbibitem{4}{\cslleftmargin{[4]}\cslrightinline{J. D. Hunter, “Matplotlib: A 2D graphics environment,” \textit{Computing in science \& engineering}, vol. 9, no. 3, pp. 90–95, 2007, doi: \href{https://doi.org/10.1109/MCSE.2007.55}{10.1109/MCSE.2007.55}.}}

\cslbibitem{5}{\cslleftmargin{[5]}\cslrightinline{S. Malhotra \textit{et al.}, “SARS-CoV-2 Reinfection Rate and Estimated Effectiveness of the Inactivated Whole Virion Vaccine BBV152 Against Reinfection Among Health Care Workers in New Delhi, India,” \textit{Jama network open}, vol. 5, no. 1, p. e2142210, Jan. 2022, doi: \href{https://doi.org/10.1001/jamanetworkopen.2021.42210}{10.1001/jamanetworkopen.2021.42210}.}}

\cslbibitem{6}{\cslleftmargin{[6]}\cslrightinline{E. Eythorsson, H. L. Runolfsdottir, R. F. Ingvarsson, M. I. Sigurdsson, and R. Palsson, “Rate of SARS-CoV-2 Reinfection During an Omicron Wave in Iceland,” \textit{Jama network open}, vol. 5, no. 8, p. e2225320, Aug. 2022, doi: \href{https://doi.org/10.1001/jamanetworkopen.2022.25320}{10.1001/jamanetworkopen.2022.25320}.}}

\cslbibitem{7}{\cslleftmargin{[7]}\cslrightinline{A. R. Guedes \textit{et al.}, “Reinfection rate in a cohort of healthcare workers over 2 years of the COVID-19 pandemic,” \textit{Scientific reports}, vol. 13, no. 1, p. 712, Jan. 2023, doi: \href{https://doi.org/10.1038/s41598-022-25908-6}{10.1038/s41598-022-25908-6}.}}

\cslbibitem{8}{\cslleftmargin{[8]}\cslrightinline{S. Medić \textit{et al.}, “Risk and severity of SARS-CoV-2 reinfections during 2020 2022 in Vojvodina, Serbia: A population-level observational study,” \textit{The lancet regional health europe}, vol. 20, Sep. 2022, doi: \href{https://doi.org/10.1016/j.lanepe.2022.100453}{10.1016/j.lanepe.2022.100453}.}}

\cslbibitem{9}{\cslleftmargin{[9]}\cslrightinline{M. E. Flacco \textit{et al.}, “Risk of reinfection and disease after SARS-CoV-2 primary infection: Meta-analysis,” \textit{European journal of clinical investigation}, vol. 52, no. 10, p. e13845, Oct. 2022, doi: \href{https://doi.org/10.1111/eci.13845}{10.1111/eci.13845}.}}

\end{cslbibliography}
\end{document}